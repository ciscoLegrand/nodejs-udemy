let number = 0
number++
console.log("number = " + number)

function greet(){
    console.log("greet =  Hola")
}
function addNumber(){
    console.log("addnumber: ")
    for(let i=0;i<5;i++){
        console.log(`iter ${i}\t=>\t${number++}\n`)
    }

}
//para poder utilizar las funciones fuera del modulo se deben exportar
module.exports= {
    greet,
    addNumber
}