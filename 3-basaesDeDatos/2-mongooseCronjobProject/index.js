const mongoose = require ('mongoose')
const { MONGO_URI } = require('./config')
const axios = require('axios').default
const cheerio = require('cheerio')
const cron = require('node-cron')

mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true})
const { BreakingNew } = require('./models')


// "0 */4 * * *" -> para que se ejecute cada 4 horas
cron.schedule("* * * * *", async() => { 
  console.log('Cronjob Executed!...');
    const html = await axios.get('https://www.marca.com/')
    //const html = await axios.get('https://cnnespanol.cnn.com/')
    const $ = cheerio.load(html.data)
    const titles = $('.mod-title')
    //const titles = $('.news__title')
    titles.each( (index,element) => {
      const breakingNew = {
        title: $(element)
                  .text()
                  .toString(),
        link: $(element)
                  .children()
                  .attr('href')
      }

      BreakingNew.create( [breakingNew] )
    })
})





