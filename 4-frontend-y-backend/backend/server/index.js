const express = require('express')
const server = express()
//habilitar cors para evitar que los request de angular sean bloqueados
const cors = require('cors')
const { Technology }= require('../models')

//configurando middlewares

// para que toda la informacion del request sea tratada como un json
server.use(express.json())
// colocar la carpeta public como una carpeta estatica 
// para qeu su contenido sea accesible sin que tenga que pasar 
// por un controlador
server.use(express.static(__dirname + "/../public"))
server.use(cors())


//configurando rutas

server.get('/api/technologies', async (req,res) => {
  let technologies = await Technology.find()
  technologies = technologies.map((technology) => {
    technology.logo = `${req.protocol}://${req.headers.host}/img/${technology.logo}`
    return technology
  }) 

  return res.send({ error: false, data: technologies })
})

server.get('/api/technology/:id', async (req,res) => {
  const { id } = req.params
  let technology = await Technology.findById(id)
  technology.logo = `${req.protocol}://${req.headers.host}/img/${technology.logo}`

  return res.send({error: false, data: technology})
})

server.get('/api/technology/search/:name', async (req,res) => {
  const { name } = req.params
  let technologies = await Technology.find({
    name: { $regex: new RegExp(name,"i")}
  })

  technologies = technologies.map(technology => {
    technology.logo = `${req.protocol}://${req.headers.host}/img/${technology.logo}`
    //Hay que devolver technology para que ofrezca una respuesta y no un array de nulls
    return technology
  })

  return res.send({error: false, data: technologies})
})

module.exports = server