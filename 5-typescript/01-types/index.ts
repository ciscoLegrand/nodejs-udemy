// types
let framework: string = "Angular";
const isProduction: boolean = true;
const PORT: number = 3000;
const styles: Array<string> = ["./css/sytle.css"]

const sayHello: Function = (name:string): string => `Hello ${name}`

const value = `${sayHello('Cisco')} estamos probando typescript pero no en ${framework} ni en el puerto ${PORT} y para asegurarnos que no estamos en produccion ${!isProduction}`

console.log(value);
