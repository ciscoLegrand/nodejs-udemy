"use strict";
//classes
var Pizza = /** @class */ (function () {
    function Pizza() {
        this.tomate = false;
        this.cheese = "";
        this.bacon = false;
        this.otherIngredients = [""];
    }
    Pizza.prototype.setCheese = function (cheese) {
        this.cheese = cheese;
        return this;
    };
    Pizza.prototype.setBacon = function () {
        this.bacon = true;
        return this;
    };
    Pizza.prototype.setOtherIngredients = function (ingredients) {
        this.otherIngredients = ingredients;
        return this;
    };
    Pizza.prototype.build = function () {
        return this;
    };
    return Pizza;
}());
var pepperoniPizza = new Pizza();
pepperoniPizza
    .setBacon()
    .setCheese("mozzarella")
    .setOtherIngredients(["Tomato", "bbq sauce"])
    .build();
console.log(pepperoniPizza);
